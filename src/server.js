const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const { readdirSync } = require("fs");
const bodyParser = require('body-parser');
require('dotenv').config();

//require db
const connectionDB = require('./database');

//swagger
const swaggerUI = require('swagger-ui-express');
const swaggerJSdoc = require('swagger-jsdoc');



//app - server
const app = express();

//db - connection
connectionDB();

//middlewares
app.use(morgan('dev'));
app.use(express.json({ limit: '2mb' }));
app.use(express.urlencoded({ extended:false }));
app.use(cors());

//routes
//opcion 1 de uso de rutas
// const routesUser = require('./routes/user')
// app.use('/', routesUser)

// routes middlewares-fs
//opcion 2
console.log(readdirSync(__dirname + "/routes"))
readdirSync(__dirname + "/routes").map((r) => app.use("/api", require("./routes/" + r)));


//port 
const port = process.env.PORT || 8000;

//listen 
app.listen(port, () => {
    console.log(`server is running on port  ${port}`)
});