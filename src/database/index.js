const mongoose = require('mongoose');

// connection function

const connecttDB = async () => {
    try {
        await mongoose.connect(process.env.DATABASE,{
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useFindAndModify:false,
            useCreateIndex:true,

        });
        console.log('DB Connected!!')
    } catch (error) {
        console.log('DB Connection error: ',error);
        process.exit(1);
    }
}

module.exports = connecttDB;